const express = require('express');
const bodyParser=require('body-parser');
const mongoose = require('mongoose');
const app = express();
const port=process.env.PORT || 3000;
const config=require('./config/config');
const cors=require('cors');
//config app
app.use(bodyParser.json());
app.use(cors());
//headers
app.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    next();
  })
//conexion database
mongoose.connect(config.db,{useNewUrlParser:true});
mongoose.connection.on('error',error=>{
    console.log('error de conexion de base de datos: ',error);
});
mongoose.connection.once('open',()=>{
    console.log('success: conexion establecida');
});

//listen
app.listen(config.port,()=>{
    console.log('listenig at localhost: '+port);
})
//import controllers
const customer=require('./controllers/user');
const branches_atms=require('./controllers/branches_atms');
const accounts=require('./controllers/accounts.js');
const movements=require('./controllers/movements.js');
const banxico=require('./controllers/banxico.js');
//inmport middlewares
const auth=require('./middleware/auth');

//-----------------------------------------APIS
app.post('/login',customer.signIn);

app.get('/atms',auth,branches_atms.getAtms);
app.get('/branches',auth,branches_atms.getBranches);

app.get('/accounts',auth,accounts.getAccounts)

app.put('/transfers',auth,accounts.transfer);
app.get('/movements',auth,movements.getMovements);
app.get('/banxico',banxico.getCotizaciones);