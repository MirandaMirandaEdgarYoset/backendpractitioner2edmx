'use strict'
const jwt = require('jwt-simple');
const moment =require('moment');
const secret="N07310d1g0Am160"

function createToken(user){
    const payload = {
        sub: user.email,
        iat: moment.unix(),
        exp: moment(new Date()).add(7, 'days').toDate()
    }
    return jwt.encode(payload,secret);
}

function decodeToken(token,customer){
    console.log(customer)
    const decode= new Promise((resolve,reject)=>{
        try{
            const payload =  jwt.decode(token,secret);
            if(payload.exp<=moment.unix()){
                reject({
                    status: 401,
                    message: "El token ha expirado"
                })
            }
            if(payload.sub!=customer){
                reject({
                    status: 403,
                    message: "No tienes autorizacion"
                })
            }
            resolve(payload.sub)
        }
        catch(err){
            reject({
                status:500,
                message:"Token Invalido"
            })
        }
    })
    return decode;
}

module.exports={
    createToken,
    decodeToken
}