'use strict'
const customer=require('../models/users');
const bcrypt=require('bcrypt');
const service = require('../services');

function signUp(request,response){
    let reqUserName=request.body.userName;
    let reqLastName=request.body.lastName;
    let reqEmail=request.body.email;
    let reqPassword=request.body.password;
    let newCustomer;

    customer.findOne({
        email:reqEmail
    },async (error,document)=>{
        if(error){
            response.status(500).send({error:error})
        }
        if(document==null){
            bcrypt.hash(reqPassword, 10,function(error,hash) {
                if(hash){
                    newCustomer = new customer({
                        userName:reqUserName,
                        lastName:reqLastName,
                        email:reqEmail,
                        password:hash,
                        accounts:[]
                    }).save((err,doc)=>{
                        if(err)
                            response.status(404).send({status:404,error:"No se pudo registrar el usuario"});
                        else
                        response.status(200).send({status:200,message:"Usuario registrado éxitosamente",id:doc.id});
                    });
                }
                
            });
        }
        if(document!=null){
            response.status(404).send({status:404,error:"Ya existe una cuenta asociada a ese correo "});
        }
    })



}

function signIn(request,response){
    response.set('Access-Control-Allow-Headers','Content-Type');
    let reqEmail = request.body.email;
    let reqPassword = request.body.password;
    customer.findOne({
        email:reqEmail
    },async (error,document)=>{
        if(error){
            response.status(500).send({error:error})
        }
        if(document==null){
            response.status(404).send({status:404,message:"Usuario no encontrado"});
        }
        if(document!=null){
            bcrypt.compare(reqPassword, document.password,function(err,result) {
                if(result)
                response.status(200).send({status:200,message:"Sesion Iniciada correctamente",user:{customerId:document.id,user:document.userName,email:document.email},token:service.createToken(document)});
                else
                response.status(404).send({status:404,message:"Usuario y contraseña incorrectos"});
            });
            }
    })
}


module.exports={
    signIn,signUp
}