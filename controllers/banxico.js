'use stric'
const apiKey = 'eca8574de4f32ff72cde3b415124ec0dbe85dc58637c134a13d15dbdc38ef7b1';
const host = 'https://www.banxico.org.mx';
const path = '/SieAPIRest/service/v1/series/SF43718,SF46410,SF46406,SF60632/datos/oportuno?token=';
const Request = require('node-fetch');

function getCotizaciones(request,response) {
    let url = host+path+apiKey;
    Request(url).then(res=> res.json())
    .then(body => {
        response.status(200).send({status:200, body:body.bmx.series});
    }).catch(error => {
        response.status(404).send({status:404, message:"Servicio no disponible"});
    });
}

module.exports ={
getCotizaciones
}