'use strict'
const branches_atms=require('../models/branches_atms.js');
const atm='ATM';
const branch='BRANCH';
function getBranches(request,response){
    branches_atms.find({type:branch},(error,document)=>{
        if(error){
            response.status(500).send({error:error});
        }
        if(document==null){
            response.status(404).send({status:404,message:"No se encontraron Sucursales"});
        }
        if(document!=null){
            response.status(200).send({status:200,data:document});
        }
    });
}

function getAtms(request,response){
    branches_atms.find({type:atm},(error,document)=>{
        if(error){
            response.status(500).send({error:error});
        }
        if(document==null){
            response.status(404).send({status:404,message:"No se encontraron ATMs"});
        }
        if(document!=null){
            response.status(200).send({status:200,data:document});
        }
    });
}

module.exports={
    getAtms,getBranches
}