'use strict'
const account=require('../models/account');
const customer = require('../models/users');
const movement=require('../models/movements');
const bcrypt=require('bcrypt');

function newAccount(request,response){
    console.log(request.body)
    response.set('Access-Control-Allow-Headers','Content-Type');
    let reqCustomerId = request.body.customerId;
    let reqDebitCard ={number:request.body.debitCard.number, nip:request.body.debitCard.nip,cvv:request.body.debitCard.cvv};
    let reqAccountNumber = request.body.accountNumber;
    let reqAmount = request.body.amount;
    let nip=reqDebitCard.nip;
    let cvv=reqDebitCard.cvv;

    bcrypt.hash(nip, 10,function(error,hashNip) {
        console.log(hashNip)
         if(hashNip){
            bcrypt.hash(cvv,10,function(err,hashCvv){
                console.log(hashCvv)
                if(hashCvv){
                   let aaount=new account({
                    customerId:reqCustomerId,
                    accountNumber:reqAccountNumber,
                    amount:reqAmount,
                    debitCard:{number:reqDebitCard.number,nip:hashNip,cvv:hashCvv}
                    }).save((err,doc)=>{
                        if(err)
                            response.status(404).send({status:404,error:"No se pudo crear la cuenta"});
                        else
                        response.status(200).send({status:200,message:"Cuenta  creada",doc:doc});
                    });
                }
            });
        }
        
    });
}

function getAccounts(request,response){
    let reqCustomer = request.headers.customer;
    customer.findOne({email:reqCustomer},(error,document)=>{
        if(error){
            response.status(500).send({error:error})
        }
        if(document==null){
            response.status(404).send({status:404,message:"Usuario no encontrado"});
        }
        if(document!=null){
            account.find({customerId:document.id},(error,documents)=>{
                if(error){
                    response.status(500).send({error:error})
                }
                if(documents==null){
                    response.status(404).send({status:404,message:"No se encontraron cuentas Asociadas a este cliente"});
                }
                if(documents!=null){
                    response.status(200).send({status:200,accounts:documents});
                }
            });
        }
    });
}

function transfer(request,response){
    let reqCustomer = request.headers.customer;
    let reqAmount=request.body.amount;
    let reqDate=request.body.date;
    let sendAccount = request.body.sendAccount;
    let receiveAccount = request.body.receiveAccount;
    let reqPassword=request.body.password;

    customer.findOne({email:reqCustomer},(error,document)=>{
        if(error){
            response.status(500).send({error:error})
        }
        if(document==null){
            response.status(404).send({status:404,message:"No se pudo completar la transferencia"});
        }
        if(document!=null){
            bcrypt.compare(reqPassword, document.password,function(err,result) {
                if(result){
                    account.findOne({accountNumber:sendAccount},(error1,document1)=>{
                        if(error1){
                            response.status(500).send({error:error1})
                        }
                        if(document1==null){
                            response.status(404).send({status:404,message:"No se pudo completar la transferencia"});
                        }
                        if(document1!=null){
                            if(document1.amount<reqAmount){
                                response.status(404).send({status:404,message:"Tu saldo es insuficiente."});
                            }
                            else if(reqAmount<=0){
                                response.status(404).send({status:404,message:"Ingresa un monto mayor a cero pesos."});
                            }
                            else{
                                account.findOne({accountNumber:receiveAccount},(error2,document2)=>{
                                    if(error2){
                                        response.status(404).send({status:404,message:"No se pudo completar la transferencia"});
                                    }
                                    if(document2==null){
                                        response.status(404).send({status:404,message:"La cuenta a la que quiere transferir no existe."});
                                    }
                                    if(document2!=null){
                                        account.findOneAndUpdate({accountNumber:receiveAccount},{$inc: { amount: reqAmount }},{new:true},(error3,resp)=>{
                                            if(error3){
                                                response.status(404).send({status:404,message:"No se pudo completar la transferencia"});
                                            }else{
                                                account.findOneAndUpdate({accountNumber:sendAccount},{$inc: { amount: -reqAmount }},{new:true},(error4,resp)=>{
                                                    if(error4){
                                                        response.status(404).send({status:404,message:"No se pudo completar la transferencia"}); 
                                                    }
                                                    else
                                                    createMovement(document1.customerId,reqAmount,reqDate,sendAccount,document2.customerId,receiveAccount,response);
                                                });   
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
                else
                response.status(404).send({status:404,message:"Contraseña no válida"});
            });
        }
    })
}

function createMovement(customerId,amount,date,sendAccount,receiveCustomerId,receiveAccount,response){
    let movement1=new movement({
    customerId:customerId,
    amount:amount,
    date:date,
    receiveCustomerId:receiveCustomerId,
    receiveAccount:receiveAccount,
    sendAccount: sendAccount
    }).save((err,doc)=>{
        if(err)
            response.status(404).send({status:404,error:"Error al crear la transferencia"});
        else
            response.status(200).send({status:200,message:"Transferencia exitosa",doc:doc});
    });
}

module.exports={
    newAccount,getAccounts,transfer
}