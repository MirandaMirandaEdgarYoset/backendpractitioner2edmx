'use strict'
const customer = require('../models/users');
const movement=require('../models/movements');

function getMovements(request,response){
    let reqCustomer = request.headers.customer;
    customer.findOne({email:reqCustomer},(error,doc)=>{
        if(error){
            response.status(500).send({error:error})
        }
        if(doc==null){
            response.status(404).send({status:404,message:"Usuario no encontrado"});
        }
        if(doc!=null){
            movement.find({$or:[{customerId:doc.id},{receiveCustomerId:doc.id}]},(error,documents)=>{
                if(error){
                    response.status(500).send({error:error})
                }
                if(documents==null){
                    response.status(404).send({status:404,message:"No se encontraron movimientos"});
                }
                if(documents!=null){
                    response.status(200).send({status:200,movements:documents});
                }
            });
        }
    });
}

module.exports={getMovements};