'use strict'
const services = require('../services');

function isAuth(request,response,next){
    if(!request.headers.authorization||!request.headers.customer){
        return response.status(403).send({message:'No tines Autorización'});
    }
    const token = request.headers.authorization.split(' ')[1];
    services.decodeToken(token,request.headers.customer).then(response=>{
        console.log(response)
        request.user=response;
        next();
    }).catch(resp=>{
        response.status(resp.status).send(resp);
    });
}
module.exports=isAuth;