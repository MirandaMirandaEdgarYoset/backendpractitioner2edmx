'use strict'
const mongoose = require('mongoose');

//schemas
let branches_atms = new mongoose.Schema({
    address:{
        addressName:String,
        state:Object,
        city:String,
        zipCode:String,
        country:Object,
        geolocation:Object,
        geographicGroups:Array
     },         
     description:String,
     type:String
});
//models
module.exports=mongoose.model('branches&atm',branches_atms);