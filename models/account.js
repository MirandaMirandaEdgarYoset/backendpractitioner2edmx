'use strict'
const mongoose = require('mongoose');
let Schema=mongoose.Schema;

//schemas
let customerSchema = new mongoose.Schema({
    customerId:String,
    accountNumber:Number,
    amount:Schema.Types.Decimal128,
    debitCard:Object
});
//models
module.exports=mongoose.model('account',customerSchema);