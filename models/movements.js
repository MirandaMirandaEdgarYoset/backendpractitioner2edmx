'use strict'
const mongoose = require('mongoose');
let Schema=mongoose.Schema;
//schemas
let movement = new mongoose.Schema({
    customerId:String,
    amount:Schema.Types.Decimal128,
    date:String,
    receiveCustomerId: String,
    receiveAccount:Number,
    sendAccount: Number
});
//models
module.exports=mongoose.model('movements',movement);