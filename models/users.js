'use strict'
const mongoose = require('mongoose');

//schemas
let customerSchema = new mongoose.Schema({
    userName:String,
    lastName:String,
    email:String,
    password:String,
    accounts:Array
});
//models
module.exports=mongoose.model('customer',customerSchema);